/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"

//==============================================================================
AudioBlockCopyToIssueAudioProcessor::AudioBlockCopyToIssueAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

AudioBlockCopyToIssueAudioProcessor::~AudioBlockCopyToIssueAudioProcessor()
{
}

//==============================================================================
const String AudioBlockCopyToIssueAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool AudioBlockCopyToIssueAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool AudioBlockCopyToIssueAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool AudioBlockCopyToIssueAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double AudioBlockCopyToIssueAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int AudioBlockCopyToIssueAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int AudioBlockCopyToIssueAudioProcessor::getCurrentProgram()
{
    return 0;
}

void AudioBlockCopyToIssueAudioProcessor::setCurrentProgram (int /*index*/)
{
}

const String AudioBlockCopyToIssueAudioProcessor::getProgramName (int /*index*/)
{
    return {};
}

void AudioBlockCopyToIssueAudioProcessor::changeProgramName (int /*index*/, const String& /*newName*/)
{
}

//==============================================================================
void AudioBlockCopyToIssueAudioProcessor::prepareToPlay (double /*sampleRate*/, int samplesPerBlock)
{
	tempBuffer.setSize(getTotalNumOutputChannels(), samplesPerBlock);
}

void AudioBlockCopyToIssueAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool AudioBlockCopyToIssueAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void AudioBlockCopyToIssueAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& /*midiMessages*/)
{
    ScopedNoDenormals noDenormals;

	juce::dsp::AudioBlock<float> block{ buffer };
	juce::dsp::ProcessContextReplacing<float> context{ block };
    
	auto& inBlock = context.getInputBlock();
	inBlock.copyTo(tempBuffer);
}

//==============================================================================
bool AudioBlockCopyToIssueAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* AudioBlockCopyToIssueAudioProcessor::createEditor()
{
#if JUCE_VERSION == ((5 << 16) + (4 << 8) + 3)
    return new GenericAudioProcessorEditor (this);
#elif JUCE_VERSION == ((5 << 16) + (4 << 8) + 4)
	return new GenericAudioProcessorEditor(*this);
#endif
}

//==============================================================================
void AudioBlockCopyToIssueAudioProcessor::getStateInformation (MemoryBlock& /*destData*/)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void AudioBlockCopyToIssueAudioProcessor::setStateInformation (const void* /*data*/, int /*sizeInBytes*/)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new AudioBlockCopyToIssueAudioProcessor();
}
